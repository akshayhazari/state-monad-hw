import qualified Data.Map as Map
import Data.Maybe 
import Control.Monad.State
import Control.Monad.Trans.Class
import Control.Monad.Trans.Maybe

type Env   = Map.Map Char Int
type Error = String

data AExp = Var Char
          | Value Int
          | Plus AExp AExp
          | Mod AExp AExp
          | Mul AExp AExp

run f =(runState f ([]))

currEnv = Map.fromList[('X',2),('Y',3),('Z',0)]

-- runState (evalX (Plus (Value 2) (Var 'X'))) []
-- OR
-- run (evalX (Plus (Value 2) (Var 'X')))

evalX :: AExp -> State [String] Int
evalX (Value v)        =  do
  modify (++["Value"])
  return (v) 
evalX (Var k)          =  do
  modify (++["Var"])
  (return . fromJust . Map.lookup k $ currEnv)  
evalX (Plus ax1 ax2)   =  do
  modify (++["Plus"])
  val1 <- evalX ax1
  val2 <- evalX ax2
  return (val1 + val2)
evalX (Mul ax1 ax2)   =  do
  modify (++["Mul"])
  val1 <- evalX ax1
  val2 <- evalX ax2
  return (val1 * val2) 
  
-- runState (aEval1 (Var 'X')) (0)
-- runState (aEval1 (Mul (Var 'X')(Value 2))) (0)
-- runState (aEval1 (Mul (Var 'P')(Value 2))) (0)  
  
aEval1 :: AExp -> State Int (Either String Int)
aEval1 (Var k)          =   
     case Map.lookup k currEnv of 
       Nothing -> do
         put 0
         return (Left "Error")
       Just a ->  do 
         modify (1+)
         return ((Right a))
         
aEval1 (Value v)        =  do
  modify (1+)
  return (Right v)
  
aEval1 (Plus ax1 ax2)   = do                          
  e1<-aEval1 ax1
  e2<-aEval1 ax2
  case (e1,e2) of 
    (Left "Error",_) -> do
      put 0
      return (Left "Error")
    (_,Left "Error") -> do
      put 0
      return (Left "Error")
    (Right v1,Right v2) -> do
      modify (1+)
      return (Right (v1+v2))
aEval1 (Mul ax1 ax2)   = do                          
  e1<-aEval1 ax1
  e2<-aEval1 ax2
  case (e1,e2) of 
    (Left "Error",_) -> do 
      put 0
      return (Left "Error")
    (_,Left "Error") -> do
      put 0
      return (Left "Error")
    (Right v1,Right v2) -> do
      modify (1+)
      return (Right (v1*v2))      
  
-- runState (aEval2 (Var 'X')) (0)
-- runState (aEval2 (Mul (Var 'X')(Value 2))) (0)  
-- runState (aEval2 (Mul (Var 'P')(Value 2))) (0)  

aEval2 :: AExp -> State Int (Maybe Int)
aEval2 (Var k)          =   
     case Map.lookup k currEnv of 
       Nothing -> do
         put 0
         return (Nothing)
       Just a ->  do 
         modify (1+)
         return ((Just a))
         
aEval2 (Value v)        =  do
  modify (1+)
  return (Just v)
  
aEval2 (Plus ax1 ax2)   = do                          
  e1<-aEval2 ax1
  e2<-aEval2 ax2
  case (e1,e2) of 
    (Nothing,_) -> do
      put 0
      return (Nothing)
    (_,Nothing) -> do
      put 0
      return (Nothing)
    (Just v1,Just v2) -> do
      modify (1+)
      return (Just (v1+v2))
aEval2 (Mul ax1 ax2)   = do                          
  e1<-aEval2 ax1
  e2<-aEval2 ax2
  case (e1,e2) of 
    (Nothing,_) -> do 
      put 0
      return (Nothing)
    (_,Nothing) -> do
      put 0
      return (Nothing)
    (Just v1,Just v2) -> do
      modify (1+)
      return (Just (v1*v2))      
      
      

